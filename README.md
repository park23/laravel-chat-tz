# laravel-chat-tz

installation

copy env.example in .env

setup database connection

fill:
BROADCAST_DRIVER=pusher

PUSHER_APP_ID=YOUR_ID 

PUSHER_APP_KEY=YOUR_KEY

PUSHER_APP_SECRET=YOUR_SECRET

PUSHER_APP_CLUSTER=YOUR_CLUSTER


in resources/js/components/chatapplication.vue 
add Pusher App Key on line 96

composer require pusher/pusher-php-server

npm install pusher-js

run php artisan migrate

create 2 users

npm run prod
